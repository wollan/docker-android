## Android SDK

Use directly or as a base image for building android apps.

#### Contents

- OpenJDK 8
- Android platform 25
- Android build tools 25.0.2

#### Usage

Build apk:

```
docker run --rm -itv $PWD:/src -w /src wollan/android ./gradlew assemble
```
